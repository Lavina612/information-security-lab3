﻿namespace StreamCipher
{
    public static class StreamCipher
    {
        private static Keys keys = new Keys();

        public static byte Encrypt(byte bit)
        {
            return (byte)(bit ^ keys.GetKey());
        }

        public static byte Decrypt(byte bit)
        {
            return (byte)(bit ^ keys.GetKeyForDecrypt());
        }
    }
}
