﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StreamCipher
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter text to encrypt:");
            var textToEcnrypt = Console.ReadLine();
            Console.WriteLine();

            var bitsToEncrypt = textToEcnrypt.SelectMany(x => NumberSystemOperation.GetBinaryFromCharacterRepresentation(x));

            var encryptedBits = new List<byte>();
            foreach (var bit in bitsToEncrypt)
            {
                encryptedBits.Add(StreamCipher.Encrypt(bit));
            }

            Console.WriteLine("Encrypted bits:");
            foreach (var bit in encryptedBits)
            {
                Console.Write(bit + " ");
            }
            Console.WriteLine("\n");

            PrintTextByBits("Encrypted text:", encryptedBits);

            var decryptedBits = new List<byte>();
            foreach (var bit in encryptedBits)
            {
                decryptedBits.Add(StreamCipher.Decrypt(bit));
            }

            PrintTextByBits("Decrypted text:", decryptedBits);
        }

        private static void PrintTextByBits(string message, List<byte> bits)
        {
            Console.WriteLine("Encrypted text:");
            for (int i = 0; i < bits.Count; i = i + 8)
            {
                Console.Write((char)NumberSystemOperation.GetDecimalFromBinaryRepresentation(bits.GetRange(i, 8)));
            }
            Console.WriteLine("\n");
        }
    }
}
