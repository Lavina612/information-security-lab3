﻿using System;
using System.Collections.Generic;

namespace StreamCipher
{
    public class Keys
    {
        private List<byte> firstLRS { get; set; }

        private List<byte> secondLRS { get; set; }

        private List<byte> thirdLRS { get; set; }

        private List<byte> generatedKeys { get; set; }

        private int index = 0;

        public Keys()
        {
            var key = new List<byte>();
            firstLRS = new List<byte>();
            secondLRS = new List<byte>();
            thirdLRS = new List<byte>();
            generatedKeys = new List<byte>();

            const int firstLRSSize = 65;
            const int secondLRSSize = 68;
            const int thirdLRSSize = 80;

            Random random = new Random();
            for (int i = 0; i < thirdLRSSize; i++)
            {
                key.Add((byte)random.Next(2));
            }

            firstLRS.AddRange(key.GetRange(thirdLRSSize - firstLRSSize, firstLRSSize));
            secondLRS.AddRange(key.GetRange(thirdLRSSize - secondLRSSize, secondLRSSize));
            thirdLRS.AddRange(key);
        }

        public byte GetKey()
        {
            int sum = firstLRS[firstLRS.Count - 1] + secondLRS[secondLRS.Count - 1] + thirdLRS[thirdLRS.Count - 1];

            ShiftFirstLRS();
            ShiftSecondLRS();
            ShiftThirdLRS();

            //return GeneratedKeys[GeneratedKeys.Count - 1];
    
            generatedKeys.Add(sum >= 2 ? (byte)1 : (byte)0);
            return generatedKeys[generatedKeys.Count - 1];
        }

        private void ShiftFirstLRS()
        {
            var firstElement = (byte)(firstLRS[0] ^ firstLRS[60] ^ firstLRS[61] ^ firstLRS[63] ^ firstLRS[64]);

            for (int i = firstLRS.Count - 1; i > 0; i--)
            {
                firstLRS[i] = firstLRS[i - 1];
            }
            firstLRS[0] = firstElement;
        }

        private void ShiftSecondLRS()
        {
            var firstElement = (byte)(secondLRS[0] ^ secondLRS[62] ^ secondLRS[65] ^ secondLRS[66] ^ secondLRS[67]);

            for (int i = secondLRS.Count - 1; i > 0; i--)
            {
                secondLRS[i] = secondLRS[i - 1];
            }
            secondLRS[0] = firstElement;
        }

        private void ShiftThirdLRS()
        {
            var firstElement = (byte)(thirdLRS[0] ^ thirdLRS[75] ^ thirdLRS[76] ^ thirdLRS[77] ^ thirdLRS[79]);

            for (int i = thirdLRS.Count - 1; i > 0; i--)
            {
                thirdLRS[i] = thirdLRS[i - 1];
            }
            thirdLRS[0] = firstElement;
        }

        public byte GetKeyForDecrypt()
        {
            return generatedKeys[index++];
        }
    }
}
