﻿using System;
using System.Collections.Generic;

namespace StreamCipher
{
    public static class NumberSystemOperation
    {
        public static List<byte> GetBinaryFromCharacterRepresentation(char symbol)
        {
            var bits = new List<byte>();

            for (int i = 7; i >= 0; i--)
            {
                bits.Add((byte)((symbol >> i) & 1));
            }

            return bits;
        }

        public static int GetDecimalFromBinaryRepresentation(List<byte> bits)
        {
            var number = 0;
            for (int i = 0; i < bits.Count; i++)
            {
                number = number + bits[bits.Count - i - 1] * (int)Math.Pow(2, i);
            }

            return number;
        }
    }
}
